#include "mainwindow.h"
#include <QApplication>
#include "armbandverwaltung.h"
#include "hauptverwaltung.h"

int main(int argc, char *argv[])
{
    //Armbandverwaltung armbandVerw;
    //armbandVerw.ArmbandverwaltungTest();

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    w.launch();

    return a.exec();
}
