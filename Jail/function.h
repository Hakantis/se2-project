#ifndef FUNCTION_H
#define FUNCTION_H

#include <iostream>
#include <unistd.h>
#include <qstring.h>
#include <QFile>
#include <QTextStream>


class function{
public:
    function(){}
    bool writeFile(QString string);
    QStringList readFile();
};

#endif // FUNCTION_H
