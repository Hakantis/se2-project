#include "hauptverwaltung.h"

bool started = false;

Hauptverwaltung::Hauptverwaltung(string name):ControllerIntf(name)
{

}

void Hauptverwaltung::inject(ControllerIntf &controller) {
    if(!cintf.empty()){ //wenn es nicht leer ist
        for(size_t i = 0; i < cintf.size(); i++){
            if(cintf[i]->getName() == controller.getName()){ //wenn es existiert
                return; //nichts tun
            }
        }
    }
    cintf.push_back(&controller);
}

void Hauptverwaltung::start(){
    if(!cintf.empty() && !started){
        cout << "started: " << getName() << endl;
        for(size_t i = 0; i < cintf.size(); i++){
            cintf[i]->start();
        }
    }
    started = true;
}

void Hauptverwaltung::stop(bool exit){
    if(exit && started){
        while(!cintf.empty()){
            cintf.back()->stop(exit);
            cintf.pop_back();
        }
        cout << "stopped: " << getName() << endl;
    }
    started = false;
}
