/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QTabWidget *Traktverwaltungen;
    QWidget *Hauptverwaltung;
    QWidget *Trakt_01;
    QWidget *Trakt_02;
    QWidget *Trakt_03;
    QWidget *Trakt_04;
    QTabWidget *tab_waerter;
    QWidget *tab_waerter_aufnehmen;
    QLabel *label_waerter_vorname;
    QLabel *label_warter_nachname;
    QLineEdit *lineEdit_waerter_vorname;
    QLineEdit *lineEdit_waerter_nachname;
    QPushButton *pushButton_waerter_aufnehmen;
    QPushButton *pushButton_warter_eingabe_loeschen;
    QWidget *tab_waerter_entlassen;
    QListWidget *list_waerter;
    QTabWidget *tab_insasse;
    QWidget *tab_insasse_aufnehmen;
    QLabel *label_insasse_vorname;
    QLabel *label_insasse_nachname;
    QLineEdit *lineEdit_insasse_vorname;
    QLineEdit *lineEdit_insasse_nachname;
    QLabel *label_haftende;
    QDateEdit *dateEdit;
    QPushButton *pushButton_insasse_aufnehmen;
    QPushButton *pushButton_insasseeingabe_loeschen;
    QWidget *tab_insasse_entlassen;
    QListWidget *list_insasse;
    QFrame *frame;
    QLabel *info;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(804, 901);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        Traktverwaltungen = new QTabWidget(centralWidget);
        Traktverwaltungen->setObjectName(QStringLiteral("Traktverwaltungen"));
        Traktverwaltungen->setGeometry(QRect(20, 370, 761, 471));
        Hauptverwaltung = new QWidget();
        Hauptverwaltung->setObjectName(QStringLiteral("Hauptverwaltung"));
        Traktverwaltungen->addTab(Hauptverwaltung, QString());
        Trakt_01 = new QWidget();
        Trakt_01->setObjectName(QStringLiteral("Trakt_01"));
        Traktverwaltungen->addTab(Trakt_01, QString());
        Trakt_02 = new QWidget();
        Trakt_02->setObjectName(QStringLiteral("Trakt_02"));
        Traktverwaltungen->addTab(Trakt_02, QString());
        Trakt_03 = new QWidget();
        Trakt_03->setObjectName(QStringLiteral("Trakt_03"));
        Traktverwaltungen->addTab(Trakt_03, QString());
        Trakt_04 = new QWidget();
        Trakt_04->setObjectName(QStringLiteral("Trakt_04"));
        Traktverwaltungen->addTab(Trakt_04, QString());
        tab_waerter = new QTabWidget(centralWidget);
        tab_waerter->setObjectName(QStringLiteral("tab_waerter"));
        tab_waerter->setGeometry(QRect(20, 140, 351, 211));
        tab_waerter_aufnehmen = new QWidget();
        tab_waerter_aufnehmen->setObjectName(QStringLiteral("tab_waerter_aufnehmen"));
        label_waerter_vorname = new QLabel(tab_waerter_aufnehmen);
        label_waerter_vorname->setObjectName(QStringLiteral("label_waerter_vorname"));
        label_waerter_vorname->setGeometry(QRect(10, 10, 161, 16));
        label_warter_nachname = new QLabel(tab_waerter_aufnehmen);
        label_warter_nachname->setObjectName(QStringLiteral("label_warter_nachname"));
        label_warter_nachname->setGeometry(QRect(10, 40, 161, 16));
        lineEdit_waerter_vorname = new QLineEdit(tab_waerter_aufnehmen);
        lineEdit_waerter_vorname->setObjectName(QStringLiteral("lineEdit_waerter_vorname"));
        lineEdit_waerter_vorname->setGeometry(QRect(140, 10, 191, 20));
        lineEdit_waerter_nachname = new QLineEdit(tab_waerter_aufnehmen);
        lineEdit_waerter_nachname->setObjectName(QStringLiteral("lineEdit_waerter_nachname"));
        lineEdit_waerter_nachname->setGeometry(QRect(140, 40, 191, 20));
        pushButton_waerter_aufnehmen = new QPushButton(tab_waerter_aufnehmen);
        pushButton_waerter_aufnehmen->setObjectName(QStringLiteral("pushButton_waerter_aufnehmen"));
        pushButton_waerter_aufnehmen->setGeometry(QRect(230, 140, 101, 31));
        pushButton_warter_eingabe_loeschen = new QPushButton(tab_waerter_aufnehmen);
        pushButton_warter_eingabe_loeschen->setObjectName(QStringLiteral("pushButton_warter_eingabe_loeschen"));
        pushButton_warter_eingabe_loeschen->setGeometry(QRect(110, 140, 101, 31));
        tab_waerter->addTab(tab_waerter_aufnehmen, QString());
        tab_waerter_entlassen = new QWidget();
        tab_waerter_entlassen->setObjectName(QStringLiteral("tab_waerter_entlassen"));
        list_waerter = new QListWidget(tab_waerter_entlassen);
        list_waerter->setObjectName(QStringLiteral("list_waerter"));
        list_waerter->setGeometry(QRect(10, 10, 321, 281));
        tab_waerter->addTab(tab_waerter_entlassen, QString());
        tab_insasse = new QTabWidget(centralWidget);
        tab_insasse->setObjectName(QStringLiteral("tab_insasse"));
        tab_insasse->setGeometry(QRect(430, 140, 351, 211));
        tab_insasse_aufnehmen = new QWidget();
        tab_insasse_aufnehmen->setObjectName(QStringLiteral("tab_insasse_aufnehmen"));
        label_insasse_vorname = new QLabel(tab_insasse_aufnehmen);
        label_insasse_vorname->setObjectName(QStringLiteral("label_insasse_vorname"));
        label_insasse_vorname->setGeometry(QRect(10, 20, 161, 16));
        label_insasse_nachname = new QLabel(tab_insasse_aufnehmen);
        label_insasse_nachname->setObjectName(QStringLiteral("label_insasse_nachname"));
        label_insasse_nachname->setGeometry(QRect(10, 50, 161, 16));
        lineEdit_insasse_vorname = new QLineEdit(tab_insasse_aufnehmen);
        lineEdit_insasse_vorname->setObjectName(QStringLiteral("lineEdit_insasse_vorname"));
        lineEdit_insasse_vorname->setGeometry(QRect(140, 20, 191, 20));
        lineEdit_insasse_nachname = new QLineEdit(tab_insasse_aufnehmen);
        lineEdit_insasse_nachname->setObjectName(QStringLiteral("lineEdit_insasse_nachname"));
        lineEdit_insasse_nachname->setGeometry(QRect(140, 50, 191, 20));
        label_haftende = new QLabel(tab_insasse_aufnehmen);
        label_haftende->setObjectName(QStringLiteral("label_haftende"));
        label_haftende->setGeometry(QRect(10, 90, 131, 16));
        dateEdit = new QDateEdit(tab_insasse_aufnehmen);
        dateEdit->setObjectName(QStringLiteral("dateEdit"));
        dateEdit->setGeometry(QRect(140, 90, 191, 22));
        pushButton_insasse_aufnehmen = new QPushButton(tab_insasse_aufnehmen);
        pushButton_insasse_aufnehmen->setObjectName(QStringLiteral("pushButton_insasse_aufnehmen"));
        pushButton_insasse_aufnehmen->setGeometry(QRect(230, 140, 101, 31));
        pushButton_insasseeingabe_loeschen = new QPushButton(tab_insasse_aufnehmen);
        pushButton_insasseeingabe_loeschen->setObjectName(QStringLiteral("pushButton_insasseeingabe_loeschen"));
        pushButton_insasseeingabe_loeschen->setGeometry(QRect(110, 140, 101, 31));
        tab_insasse->addTab(tab_insasse_aufnehmen, QString());
        tab_insasse_entlassen = new QWidget();
        tab_insasse_entlassen->setObjectName(QStringLiteral("tab_insasse_entlassen"));
        list_insasse = new QListWidget(tab_insasse_entlassen);
        list_insasse->setObjectName(QStringLiteral("list_insasse"));
        list_insasse->setGeometry(QRect(10, 10, 321, 281));
        tab_insasse->addTab(tab_insasse_entlassen, QString());
        frame = new QFrame(centralWidget);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(-10, -10, 811, 141));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        info = new QLabel(frame);
        info->setObjectName(QStringLiteral("info"));
        info->setGeometry(QRect(30, 10, 761, 111));
        QFont font;
        font.setPointSize(40);
        info->setFont(font);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 804, 17));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        Traktverwaltungen->setCurrentIndex(3);
        tab_waerter->setCurrentIndex(0);
        tab_insasse->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        Traktverwaltungen->setTabText(Traktverwaltungen->indexOf(Hauptverwaltung), QApplication::translate("MainWindow", "Hauptverwaltung", nullptr));
        Traktverwaltungen->setTabText(Traktverwaltungen->indexOf(Trakt_01), QApplication::translate("MainWindow", "Trakt_01", nullptr));
        Traktverwaltungen->setTabText(Traktverwaltungen->indexOf(Trakt_02), QApplication::translate("MainWindow", "Trakt_02", nullptr));
        Traktverwaltungen->setTabText(Traktverwaltungen->indexOf(Trakt_03), QApplication::translate("MainWindow", "Trakt_03", nullptr));
        Traktverwaltungen->setTabText(Traktverwaltungen->indexOf(Trakt_04), QApplication::translate("MainWindow", "Trakt_04", nullptr));
        label_waerter_vorname->setText(QApplication::translate("MainWindow", "Vorname:", nullptr));
        label_warter_nachname->setText(QApplication::translate("MainWindow", "Nachname:", nullptr));
        pushButton_waerter_aufnehmen->setText(QApplication::translate("MainWindow", "Aufnehmen", nullptr));
        pushButton_warter_eingabe_loeschen->setText(QApplication::translate("MainWindow", "Zur\303\274cksetzen", nullptr));
        tab_waerter->setTabText(tab_waerter->indexOf(tab_waerter_aufnehmen), QApplication::translate("MainWindow", "Waerter aufnehmen", nullptr));
        tab_waerter->setTabText(tab_waerter->indexOf(tab_waerter_entlassen), QApplication::translate("MainWindow", "Waerter entlassen", nullptr));
        label_insasse_vorname->setText(QApplication::translate("MainWindow", "Vorname:", nullptr));
        label_insasse_nachname->setText(QApplication::translate("MainWindow", "Nachname:", nullptr));
        label_haftende->setText(QApplication::translate("MainWindow", "Haftende:", nullptr));
        pushButton_insasse_aufnehmen->setText(QApplication::translate("MainWindow", "Aufnehmen", nullptr));
        pushButton_insasseeingabe_loeschen->setText(QApplication::translate("MainWindow", "Zur\303\274cksetzen", nullptr));
        tab_insasse->setTabText(tab_insasse->indexOf(tab_insasse_aufnehmen), QApplication::translate("MainWindow", "Insasse aufnehmen", nullptr));
        tab_insasse->setTabText(tab_insasse->indexOf(tab_insasse_entlassen), QApplication::translate("MainWindow", "Insasse entlassen", nullptr));
        info->setText(QApplication::translate("MainWindow", "Info", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
