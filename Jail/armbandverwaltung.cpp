#include "armbandverwaltung.h"

Armbandverwaltung::Armbandverwaltung()
{

}
/**
 * @brief generiert eine individuelle id
 * @param type  (w- wärter; i- insasse)
 * @return individuelle id
 */
QString Armbandverwaltung::generateId(char type){
    srand(time(NULL));
    QString sId;

    int nId = rand() % 999999 + 1000000; //random id number
    sId = type + sId.setNum(nId);

    return sId;
}

int Armbandverwaltung::ArmbandverwaltungTest()
{
    char cWaerter = 'w';
    char cInsasse = 'i';

    QString sWaerterId = generateId(cWaerter);
    sleep(5);
    QString sInsasseId = generateId(cInsasse);

    cout << " Wärter ID: " << sWaerterId.toStdString() << endl;
    cout << " Insasse ID: " << sInsasseId.toStdString() << endl;

    return 0;
}
