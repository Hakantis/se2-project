#ifndef ARMBANDVERWALTUNG_H
#define ARMBANDVERWALTUNG_H

#include <iostream>
#include <time.h>
#include <unistd.h>
#include <qstring.h>
#include "component.h"

using namespace std;

class Armbandverwaltung
{

public:
    Armbandverwaltung();
    QString generateId(char type);
    int ArmbandverwaltungTest();
};

#endif // ARMBANDVERWALTUNG_H
