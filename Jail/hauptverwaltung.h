#ifndef HAUPTVERWALTUNG_H
#define HAUPTVERWALTUNG_H

#include "ControllerIntf.h"
#include "personenverwaltung.h"
#include "armbandverwaltung.h"
#include "tuerverwaltung.h"

class Hauptverwaltung:public ControllerIntf
{
public:
    Hauptverwaltung(string name);
    void inject(ControllerIntf &controller);
    void start();
    void stop(bool exit = true);

private:
    vector<ControllerIntf*> cintf;
};

#endif // HAUPTVERWALTUNG_H
