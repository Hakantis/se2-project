#include "component.h"

Component::Component(string name):ControllerIntf(name){

}

Component::~Component(){

}
Component::Component(){

}

/**
 * @description ausgabe des erfolgreichen start eines
 * component objects mit seinem Namen
 */
void Component::start(){
    cout << "started: " << getName() << endl;
}

/**
 * @description ausgabe des erfolgreichen stops eines
 * component objects mit seinem Namen
 */
void Component::stop(bool exit){
    if(exit){
        cout << "stopped: " << getName() << endl;
    }
}
