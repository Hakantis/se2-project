#include "function.h"
#define dateiPfad "C:/Qt/qt-workspace/se2-project/Jail/list.txt"

bool writeFile(QString string){
    QString dateiname = dateiPfad;
    QFile file(dateiname);

    if(file.open(QIODevice::WriteOnly)){
        QTextStream stream(&file);
        stream << string << endl;
        file.close();
        return true;
    }
    return false;
}

QStringList readFile(){
    QString dateiname = dateiPfad;
    QFile file(dateiname);
    QStringList liste;

    if(file.open(QIODevice::ReadOnly)){
        QTextStream stream(&file);
        while(!stream.atEnd()){
            QString line = stream.readLine();
            liste = line.split("\n");
        }
    }
    file.close();
    return liste;
}
