#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <unistd.h>
#include <map>
static QString waerterListe("C:/Qt/qt-workspace/se2-project/Jail/WaerterList.txt");
static QString insasseListe("C:/Qt/qt-workspace/se2-project/Jail/InsasseList.txt");

/**
  * @brief gui und funtionen
  * @author hakan türeyen
  */

/**
 * @brief konstruktor welcher insasse und wärter liste liest
 * @param parent
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->list_waerter->addItems(readFile(waerterListe));
    ui->list_insasse->addItems(readFile(insasseListe));
}

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * @brief löscht die labeleingaben
 */
void MainWindow::on_pushButton_warter_eingabe_loeschen_clicked()
{
    ui->lineEdit_waerter_vorname->clear();  //eingabe für vorname löschen
    ui->lineEdit_waerter_nachname->clear(); //eingabe für nachname löschen
    if(ui->lineEdit_waerter_vorname->text() == "" && ui->lineEdit_waerter_nachname->text() == ""){ //wenn vor-und nachname vorhanden sind
        ui->info->setText("Zurückgesetzt");        //infolabel text
        ui->info->setStyleSheet("background-color: green; text-align: center;"); //hintergrund der infolabel ändern
    }else{  //es wurde kein vor oder nachname einegeebn
        ui->info->setText("Nicht Zurückgesetzt");
        ui->info->setStyleSheet("background-color: red; text-align: center;");
    }
}

/**
 * @brief löscht die insassen eingabe
 */
void MainWindow::on_pushButton_insasseeingabe_loeschen_clicked()
{
    ui->lineEdit_insasse_vorname->clear();  //eingabe für vorname löschen
    ui->lineEdit_insasse_nachname->clear(); //eingabe für nachname löschen
    if(ui->lineEdit_insasse_vorname->text() == "" && ui->lineEdit_insasse_nachname->text() == ""){  //wenn vor-und nachname vorhanden sind
        ui->info->setText("Zurückgesetzt");
        ui->info->setStyleSheet("background-color: green; text-align: center;");
    }else{
        ui->info->setText("Nicht Zurückgesetzt");
        ui->info->setStyleSheet("background-color: red; text-align: center;");
    }
}

/**
 * @brief funktioniert nicht
 * @param lineEdit
 */
void MainWindow::resetLineEdits(QLineEdit *lineEdit[]){
    int i = 0;
    while(*lineEdit){
        lineEdit[i]->clear();
        i++;
    }
}

/**
 * @brief schreibt in eine datei
 * @param sDateipfad    string des dateipfads
 * @param string        text welche in die datei kommen soll
 * @return (true- schreiben geklappt;  false- nicht geklappt)
 */
bool MainWindow::writeFile(QString sDateipfad, QString string){
    QFile file(sDateipfad);

    if(file.open(QIODevice::WriteOnly | QIODevice::Append)){
        QTextStream stream(&file);
        stream << string << endl;
        file.close();
        return true;
    }
    return false;
}

/**
 * @brief liest eine datei
 * @param sDateipfad    string des Dateipfads
 * @return QStringList  text welcher in der datei steht
 */
QStringList MainWindow::readFile(QString sDateipfad){
    QFile file(sDateipfad);
    QStringList liste;

    if(file.open(QIODevice::ReadOnly)){
        QTextStream stream(&file);
        while(!stream.atEnd()){
            QString line = stream.readLine();
            liste << line.split("\n");
        }
    }
    file.close();
    return liste;
}

/**
 * @brief nimmt die wärter eingaben und packt ihn in die liste
 */
void MainWindow::on_pushButton_waerter_aufnehmen_clicked()
{
    Armbandverwaltung armbandVerw;
    QString sVorname = ui->lineEdit_waerter_vorname->text();
    QString sNachname = ui->lineEdit_waerter_nachname->text();
    QStringList liste;
    QString sId;

    if(sVorname != "" && sNachname != ""){  //wenn vor und nachname ausgefüllt sind
        sId = armbandVerw.generateId('w');  //id kennzeichen mit w versehen
        QString string = sNachname + ", " + sVorname + "--- " + sId;
        liste = (QStringList() << string);
        ui->list_waerter->addItems(liste);  //Qwidgetlist die werte übergeben
        if(writeFile(waerterListe, string)){    //wenn in datei schreiben möglich ist
            ui->info->setText("Willkommen im Team");
            ui->info->setStyleSheet("background-color: green; text-align: center;");
            ui->lineEdit_waerter_vorname->clear();
            ui->lineEdit_waerter_nachname->clear();
        }
    }
}

/**
 * @brief nimmt die wärter eingaben und packt ihn in die liste
 */
void MainWindow::on_pushButton_insasse_aufnehmen_clicked()
{
    Armbandverwaltung armbandVerw;
    QString sVorname = ui->lineEdit_insasse_vorname->text();
    QString sNachname = ui->lineEdit_insasse_nachname->text();
    QString sHaftEnde = ui->dateEdit->time().toString("dd.MM.yyyy");

    QStringList liste;
    QString sId;

    if(sVorname != "" && sNachname != ""){
        sId = armbandVerw.generateId('i');
        QString string = sNachname + ", " + sVorname + "--- " + sId + "/" + sHaftEnde;
        liste = (QStringList() << string);
        ui->list_insasse->addItems(liste);
        if(writeFile(insasseListe, string)){
            ui->info->setText("Willkommen im Team");
            ui->info->setStyleSheet("background-color: green; text-align: center;");
            ui->lineEdit_insasse_vorname->clear();
            ui->lineEdit_insasse_nachname->clear();
        }
    }
}


void MainWindow::einsperren(){

}


void MainWindow::launch(){
    /*Hauptverwaltung &hv = *new Hauptverwaltung("Hauptverwaltung");
    Armbandverwaltung &av = *new Armbandverwaltung("Armbandverwaltung");
    Personenverwaltung &pv = *new Personenverwaltung("Personenverwaltung");
    Tuerverwaltung &tv = *new Tuerverwaltung("Tuerverwaltung");

    hv.inject(av);
    hv.inject(pv);
    hv.inject(tv);

    hv.start();*/
}












