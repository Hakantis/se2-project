#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "function.h"
#include <QLineEdit>
#include "armbandverwaltung.h"
#include <qstring.h>
#include <QDateTime>
#include "hauptverwaltung.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    bool writeFile(QString sDateipfad, QString string);
    QStringList readFile(QString sDateipfad);
    void einsperren();
    void launch();

private slots:
    void on_pushButton_warter_eingabe_loeschen_clicked();

    void on_pushButton_insasseeingabe_loeschen_clicked();

    void on_pushButton_waerter_aufnehmen_clicked();

    void on_pushButton_insasse_aufnehmen_clicked();

private:
    Ui::MainWindow *ui;

    void resetLineEdits(QLineEdit *lineEdit[]);
};

#endif // MAINWINDOW_H
