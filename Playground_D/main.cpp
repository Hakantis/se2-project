#include <QCoreApplication>
#include "../designs/Model/MainController.h"
#include "../designs/Model/component.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    MainController &main = *new MainController("MainController");
    Component &ca = *new Component("Component_A");
    Component &cb = *new Component("Component_B");
    Component &cc = *new Component("Component_C");

    main.inject(ca);
    main.inject(cb);
    main.inject(cb);
    main.inject(cc);
    cout << "----------" << endl;
    main.start();
    main.start();
    main.stop();
    main.stop();

    return a.exec();
}
