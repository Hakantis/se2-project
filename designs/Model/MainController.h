/**
 * Project Untitled
 */


#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include "ControllerIntf.h"

class MainController:public ControllerIntf{
    public:
        MainController(string name);
        ~MainController();
        void inject(ControllerIntf &controller);
        void start();
        void stop(bool exit = true);

    private:
        vector<ControllerIntf*> cintf;
};

#endif //_MAINCONTROLLER_H
