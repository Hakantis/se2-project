/**
 * Project Untitled
 */


#ifndef CONTROLLERINTF_H
#define CONTROLLERINTF_H

#include "Interface1.h"
#include <iostream>
#include <vector>
using namespace std;

class ControllerIntf: public Interface1 {
    public:
    
    ControllerIntf(string name);
    virtual ~ControllerIntf();

    virtual void start();
    virtual void stop(bool exit);
    string getName();

    private:
        string name;
};

#endif //CONTROLLERINTF_H
