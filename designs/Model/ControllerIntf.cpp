/**
 * Project Untitled
 */


#include "ControllerIntf.h"

/**
 * ControllerIntf implementation
 */


/**
 * @param name
 */
ControllerIntf::ControllerIntf(string name):name(name) {

}

ControllerIntf::~ControllerIntf(){}

/**
 * @return void
 */
void ControllerIntf::start() {

}

/**
 * @param exit
 * @return void
 */
void ControllerIntf::stop(bool exit) {

}

/**
 * @return String
 */
string ControllerIntf::getName() {
    return name;
}
