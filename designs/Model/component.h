#ifndef COMPONENT_H
#define COMPONENT_H

#include <iostream>
#include "ControllerIntf.h"
using namespace std;

class Component:public ControllerIntf{
public:
    Component(string name);
    ~Component();

    void start();
    void stop(bool exit);
};

#endif // COMPONENT_H
