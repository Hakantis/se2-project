/**
 * Project Untitled
 */


#include "MainController.h"

/**
 * MainController implementation
 */

bool started = false;

MainController::MainController(string name):ControllerIntf(name) {

}

MainController::~MainController(){}

void MainController::inject(ControllerIntf &controller) {
    if(!cintf.empty()){ //wenn es nicht leer ist
        for(size_t i = 0; i < cintf.size(); i++){
            if(cintf[i]->getName() == controller.getName()){ //wenn es existiert
                return; //nichts tun
            }
        }
    }
    cintf.push_back(&controller);
}

void MainController::start(){
    if(!cintf.empty() && !started){
        cout << "started: " << getName() << endl;
        for(size_t i = 0; i < cintf.size(); i++){
            cintf[i]->start();
        }
    }
    started = true;
}

void MainController::stop(bool exit){
    if(exit && started){
        while(!cintf.empty()){
            cintf.back()->stop(exit);
            cintf.pop_back();
        }
        cout << "stopped: " << getName() << endl;
    }
    started = false;
}









